use ndarray::Array2;

pub trait RMQ<'a, I: PartialOrd> {
    fn new(a: &'a [I]) -> Self;
    fn rmq(&self, i: usize, j: usize) -> Option<&'a I>;
}

pub struct RMQNaive<'a, I: Ord> {
    data: Array2<Option<&'a I>>,
}

impl<'a, I: Ord> RMQ<'a, I> for RMQNaive<'a, I> {
    fn new(a: &'a [I]) -> Self {
        let n = a.len();
        let mut data = Array2::default((n, n));
        for ((i, j), min) in data.indexed_iter_mut() {
            if i > j {
                continue;
            }
            *min = a[i..=j].iter().min();
        }
        RMQNaive { data }
    }

    fn rmq(&self, i: usize, j: usize) -> Option<&'a I> {
        *self.data.get((i, j))?
    }
}

pub struct RMQQuadratic<'a, I: PartialOrd> {
    data: Array2<Option<&'a I>>,
}

impl<'a, I: Ord> RMQ<'a, I> for RMQQuadratic<'a, I> {
    fn new(a: &'a [I]) -> Self {
        let n = a.len();
        let mut data = Array2::default((n, n));
        // #TODO: rewrite with less array indexing?
        for i in 0..n {
            let mut minimum = &a[i];
            for j in (i + 1)..n {
                minimum = std::cmp::min(&a[j], minimum);
                data[(i, j)] = Some(minimum);
            }
        }
        RMQQuadratic { data }
    }

    fn rmq(&self, i: usize, j: usize) -> Option<&'a I> {
        *self.data.get((i, j))?
    }
}

/// computes `ceil(log_2(i))` for all i from 1 to n.
///
/// `log(0) := 0` as a default value with no meaning.
fn precomupte_log(n: usize) -> Vec<usize> {
    let mut log = vec![0usize; n + 1];

    // pow_2 == 2**exponent
    let mut pow_2 = 1;
    let mut exponent = 0;
    for (i, log_i) in (0..=n).zip(log.iter_mut()) {
        if i > pow_2 {
            // pow_2 == 2**exponent
            pow_2 *= 2;
            exponent += 1;
        }
        *log_i = exponent;
    }
    log
}

pub struct RMQNLogN<'a, I: PartialOrd> {
    young_tableau: Array2<Option<&'a I>>,
    log: Vec<usize>,
}
impl<'a, I: Ord> RMQ<'a, I> for RMQNLogN<'a, I> {
    fn new(a: &'a [I]) -> Self {
        let n = a.len();

        let log = precomupte_log(n);

        let log_n = log[n];

        let mut young_tableau = Array2::default((n, log_n + 1));
        // #TODO: rewrite with less array indexing?
        for i in 0..n {
            young_tableau[(i, 0)] = Some(&a[i]);
        }
        // #TODO: rewrite with less array indexing?
        for i in 0..n {
            for k in 1..=log_n {
                let offset = 1 << (k - 1); // 1<<(k-1) == (2**k) / 2
                if i + offset >= n {
                    continue;
                }
                let minimum = young_tableau[(i, k - 1)]
                    .into_iter()
                    .chain(young_tableau[(i + offset, k - 1)])
                    .min();
                young_tableau[(i, k)] = minimum;
            }
        }
        RMQNLogN { young_tableau, log }
    }

    fn rmq(&self, i: usize, j: usize) -> Option<&'a I> {
        if j < i {
            return None;
        }
        // |i   k   j|
        // |---------|
        let length = j - i;
        let log_length = self.log[length];
        let half_length = 1 << log_length; // 2**log_length
        let min1 = self.young_tableau.get((i, log_length)).copied()?;
        let min2 = self
            .young_tableau
            .get((j - half_length, log_length))
            .copied()?;
        min1.into_iter().chain(min2).min()
    }
}

//pub struct PlusMinus1RMQ {}
//impl RMQ<u32> for PlusMinus1RMQ {}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_naive_rmq() {
        let a = [3, 1, 6];
        let rmq = RMQNaive::new(&a);
        assert_eq!(rmq.rmq(1, 2), Some(&1));
    }

    #[test]
    fn test_quadratic_rmq() {
        let a = [3, 1, 6];
        let rmq = RMQQuadratic::new(&a);
        assert_eq!(rmq.rmq(1, 2), Some(&1));
    }

    #[test]
    fn test_nlogn_rmq() {
        let a = [3, 1, 6];
        let rmq = RMQNLogN::new(&a);
        println!("{:?}", rmq.young_tableau);
        assert_eq!(rmq.rmq(1, 2), Some(&1));
    }

    #[test]
    fn test_nlogn_rmq2() {
        let a = [3, 1, 6, 4, 3];
        let rmq = RMQNLogN::new(&a);
        println!("{:?}", rmq.young_tableau);
        println!("{:?}", rmq.log);
        assert_eq!(rmq.rmq(3, 5), Some(&3));
    }
}
