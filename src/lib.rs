//! # stringology
//!
//! `stringology` is a collection of algorithms used in the field of
//! [Stringology](https://en.wikipedia.org/wiki/String_(computer_science)#String_processing_algorithms).

pub mod pattern_matching;
pub mod rmq;
pub mod suffix_array;
