//! The main functionalty of this module is provided by [`suffix_array`],
//! which runs in linear time complexity.
use katexit::katexit;
use radsort;
use std::convert::TryFrom;
use std::fmt::{Debug, Display};
use std::ops::Deref;

/// A value of type `SACharacter` should be sortable using radixsort (see [`radsort::Key`]).
///
/// Further, it should be unsigned and have a smallest element, i.e. a zero element.
pub trait SACharacter: radsort::Key + PartialEq + Eq + PartialOrd + Ord + Debug + Display {
    /// provides the 0 element of the type
    fn char_zero() -> Self;
}
impl SACharacter for usize {
    #[inline]
    fn char_zero() -> Self {
        0
    }
}
impl SACharacter for u8 {
    #[inline]
    fn char_zero() -> Self {
        0
    }
}
impl SACharacter for u16 {
    #[inline]
    fn char_zero() -> Self {
        0
    }
}
impl SACharacter for u32 {
    #[inline]
    fn char_zero() -> Self {
        0
    }
}
impl SACharacter for u64 {
    #[inline]
    fn char_zero() -> Self {
        0
    }
}
impl SACharacter for u128 {
    #[inline]
    fn char_zero() -> Self {
        0
    }
}
impl SACharacter for bool {
    #[inline]
    fn char_zero() -> Self {
        false
    }
}
impl SACharacter for char {
    #[inline]
    fn char_zero() -> Self {
        '\0'
    }
}

#[inline]
fn slice_padding<const N: usize, C: SACharacter>(input: &[C]) -> [C; N] {
    let mut output = [C::char_zero(); N];
    for (&inp, out) in input.iter().zip(output.iter_mut()) {
        *out = inp;
    }
    output
}

/// The characters of a word are devided into 3 buckets:
/// w[i] belongs to bucket B[k], iff i%3 == k
#[derive(PartialEq, Eq, Debug, Clone, Copy)]
enum Bucket {
    // bucket number 0 is not used in the implementation
    //B0,
    /// bucket number 1
    B1,
    /// bucket number 2
    B2,
}

/// holds a key of some type T, and an index
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
struct KeyWithIndex<C: SACharacter> {
    key: (C, C, C),
    meta_index: usize,
    word_index: usize,
    word_bucket: Bucket,
}

/// build a meta-word, consisting of 3-tuple letters
fn build_meta_word<C: SACharacter>(w: &[C]) -> Vec<KeyWithIndex<C>> {
    let chunks_1 = w[1..].chunks_exact(3);
    let chunks_1_rem: [C; 3] = slice_padding::<3, C>(chunks_1.remainder());
    let chunks_1_rem_is_empty = chunks_1.remainder().is_empty();
    let m_1 = chunks_1
        // if the chunks_2 iterator missed the tail, then append the tail,
        // else: do not append a full 0 tail
        .chain(if chunks_1_rem_is_empty {
            None
        } else {
            Some(&chunks_1_rem[..])
        })
        .enumerate()
        .map(|(idx, elem)| (3 * idx + 1, Bucket::B1, elem));

    let chunks_2 = w[2..].chunks_exact(3);
    let chunks_2_rem: [C; 3] = slice_padding::<3, C>(chunks_2.remainder());
    let chunks_2_rem_is_empty = chunks_2.remainder().is_empty();
    let m_2 = chunks_2
        // if the chunks_2 iterator missed the tail, then append the tail,
        // else: do not append a full 0 tail
        .chain(if chunks_2_rem_is_empty {
            None
        } else {
            Some(&chunks_2_rem[..])
        })
        .enumerate()
        .map(|(idx, elem)| (3 * idx + 2, Bucket::B2, elem));

    m_1.chain(m_2)
        .enumerate()
        .map(|(meta_index, (word_index, word_bucket, a))| KeyWithIndex {
            key: (a[0], a[1], a[2]),
            meta_index,
            word_index,
            word_bucket,
        })
        .collect()
}

/// reduces a word (here: the meta-word) to an integer alphbet [1..=n],
/// where `n == m.len()`
///
/// assumption: `w.is_empty() == false` holds for `w`.
fn reduce_word_to_integer_alphabet<C: SACharacter>(m: &[KeyWithIndex<C>]) -> Vec<usize> {
    debug_assert!(!m.is_empty());

    // sort the meta-word
    let mut m_sort = m.iter().collect::<Vec<_>>();
    radsort::sort_by_key(&mut m_sort, |elem| elem.key);

    // create a reduced word from the sorted meta-word
    let mut w_reduced = vec![0; m_sort.len()];
    let mut k = 1;
    let mut prev_elem = m_sort[0];
    w_reduced[prev_elem.meta_index] = 1;
    for &elem in &m_sort[1..] {
        if elem.key != prev_elem.key {
            k += 1;
        }
        w_reduced[elem.meta_index] = k;

        prev_elem = elem;
    }

    w_reduced
}

/// precondition: w contains letters only from the range `[1..=n]`,
/// with `n == w.len()`
///
/// checks, if `w` is a permutation of the set [1..=n], i.e.
/// if `w` contains each number 1, 2,...,n exactly once.
fn word_is_permutation(w: &Vec<usize>) -> bool {
    debug_assert_eq!(
        w.iter().min(),
        Some(&1),
        "The minimum entry {:?} in w = {:?} is not 1",
        w,
        w.iter().max()
    );
    debug_assert!(
        w.iter().max().expect("w should not be empty") <= &w.len(),
        "The length of w = {:?} does not match with the maximum entry {:?} in w",
        w,
        w.iter().max()
    );
    let mut letter_exists = vec![false; w.len()];
    for letter in w {
        letter_exists[letter - 1] = true;
    }
    letter_exists.into_iter().all(|exists| exists)
}

/// The SuffixArray Struct is a thin warpper around a vector.
/// It provides the following guarantee:
///
/// - The cotained vector only contains values from the range [0..n], with n the length of the
/// vector.
///
/// The struct does not guarantee, that the vector is a proper permutation.
#[derive(PartialEq, Eq, Debug)]
pub struct SuffixArray(Vec<usize>);

impl Deref for SuffixArray {
    type Target = Vec<usize>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl TryFrom<Vec<usize>> for SuffixArray {
    type Error = &'static str;

    fn try_from(value: Vec<usize>) -> Result<Self, Self::Error> {
        let n = value.len();
        if value.iter().all(|&elem| elem < n) {
            Ok(SuffixArray(value))
        } else {
            Err("The given value vector contains elements that are too large")
        }
    }
}

#[katexit]
/// Rank array to a [`SuffixArray`] of a word `w`.
///
/// The SuffixRank denotes the ordering of all suffixes of a given word,
/// and is the inverse permutation to the [`SuffixArray`] of $w$.
///
/// Given a word $w = w_0 w_1 w_2 ... w_n$,
/// the SuffixRank entry `rank[i] == j` denotes,
/// that given an ordered list of all suffixes of $w$,
/// the suffix $w_i ... w_n$ is the j.th suffix in that list.
#[derive(PartialEq, Eq, Clone, Debug)]
pub struct SuffixRank<'a> {
    sa: &'a SuffixArray,
    rank: Vec<usize>,
}

impl<'a> SuffixRank<'a> {
    /// Create the SuffixRank of the given [`SuffixArray`].
    pub fn new(sa: &'a SuffixArray) -> Self {
        let mut rank = vec![0; sa.len()];

        for (i, &sa_i) in sa.iter().enumerate() {
            rank[sa_i] = i;
        }

        SuffixRank { sa, rank }
    }
}

impl<'a> Deref for SuffixRank<'a> {
    type Target = Vec<usize>;
    fn deref(&self) -> &Self::Target {
        &self.rank
    }
}

/// Given the metaword m, and the corresopnding rank array of m, partial rank,
/// construct a rank array for the word w (enlarged_rank).
/// The enlarged_rank is only well-defined on indices i s.t. i%3 != 0.
/// For i%3==0, the enlarged_rank has value 0.
fn partial_rank_to_enlarged_rank<C: SACharacter>(
    partial_rank: &SuffixRank<'_>,
    w: &[C],
    m: &[KeyWithIndex<C>],
) -> Vec<usize> {
    debug_assert!(
        partial_rank.len() == m.len(),
        "The partial rank and the meta word m do not have the same length."
    );
    let mut enlarged_rank = vec![0; w.len() + 3];
    for (&rank_i, m_i) in partial_rank.iter().zip(m) {
        // shift rank_i up by 1,
        // such that rank_i != 0 is guaranteed.
        enlarged_rank[m_i.word_index] = rank_i + 1;
    }
    enlarged_rank
}

/// takes two sorted lists, and merges them.
///
/// `a`: first sorted iterator
///
/// `b`: second sorted iterator
///
/// *important*: a and b are assumed to be sorted,
/// the function does not sort a or b.
///
/// `a_leq_b`: predicate to tell, if $a \\leq b$
///
/// `extract_a`: extracts from A a value of type C
///
/// `extract_b`: extracts from A a value of type C
fn merge_sorted_lists_generic<IA, IB, A, B, F, FA, FB, C>(
    a: IA,
    b: IB,
    a_leq_b: F,
    extract_a: FA,
    extract_b: FB,
) -> Vec<C>
where
    IA: ExactSizeIterator<Item = A>,
    IB: ExactSizeIterator<Item = B>,
    F: Fn(&A, &B) -> bool,
    FA: Fn(A) -> C,
    FB: Fn(B) -> C,
{
    let mut a = a.peekable();
    let mut b = b.peekable();

    let mut out = Vec::with_capacity(a.len() + b.len());

    loop {
        match (a.peek(), b.peek()) {
            (Some(a_val), Some(b_val)) => {
                if a_leq_b(a_val, b_val) {
                    out.push(extract_a(a.next().unwrap()));
                } else {
                    out.push(extract_b(b.next().unwrap()));
                }
            }
            (Some(_), None) => out.push(extract_a(a.next().unwrap())),
            (None, Some(_)) => out.push(extract_b(b.next().unwrap())),
            (None, None) => break,
        }
    }
    out
}

/// `b0`: vector of tuples `(idx0, w0)` where idx0 is the index of letter `w0` in `w`
///
/// `b12`: a (partial) suffix array over the reduced meta-word
///
/// `meta_word`: the meta-word
///
/// `rank`: a partial rank array, retrieved from [`partial_rank_to_enlarged_rank`]
///
/// `w`: the full word
fn merge_sorted_lists<C: SACharacter>(
    b0: &[(usize, C)],
    b12: &SuffixArray,
    meta_word: &[KeyWithIndex<C>],
    rank: &[usize],
    w: &[C],
) -> Vec<usize> {
    let b0_iter = b0.iter();
    let b12_iter = b12.iter().map(|&idx| meta_word[idx]);

    merge_sorted_lists_generic(
        b0_iter,
        b12_iter,
        |&x, &y| {
            let (i, w_i) = x;
            debug_assert!(i % 3 == 0, "The index {} is not divisable by 3", i);
            if y.word_bucket == Bucket::B1 {
                let j = y.word_index;
                let w_j = y.key.0;
                debug_assert!(w_j == w[j]);

                let rank_jp1 = rank[y.word_index + 1];

                (*w_i, rank[i + 1]) <= (w_j, rank_jp1)
            } else {
                // y.word_bucket == Bucket::B2

                let w_ip1 = if *i + 1 < w.len() {
                    w[i + 1]
                } else {
                    C::char_zero()
                };

                let j = y.word_index;
                let n = w.len();

                let w_j = y.key.0;
                let w_jp1 = y.key.1;
                debug_assert!(w_j == w[j]);
                debug_assert!(j + 1 >= n || w_jp1 == w[j + 1]);

                (*w_i, w_ip1, rank[i + 2]) <= (w_j, w_jp1, rank[j + 2])
            }
        },
        |x| x.0,
        |y| y.word_index,
    )
}

#[katexit]
/// Create a suffix array.
///
/// The word `w` shall be over an alphabet $\\{1, ..., n\\}$.
///
/// This implementation follows an algorithm presented in this paper:
///
/// Juha Kärkkäinen, Peter Sanders, and Stefan Burkhardt. 2006.
/// Linear work suffix array construction. J. ACM 53, 6 (November 2006), 918–936.
/// <https://doi.org/10.1145/1217856.1217858>
///
/// Complexity: $\\mathcal{O}(|w|)$
pub fn suffix_array<C: SACharacter>(w: &[C]) -> SuffixArray {
    // nothing to do
    if w.len() <= 1 {
        return SuffixArray(vec![0; w.len()]);
    }

    let m: Vec<KeyWithIndex<C>> = build_meta_word(w);

    let partial_sa = {
        let mut w_reduced = reduce_word_to_integer_alphabet(&m);

        if word_is_permutation(&w_reduced) {
            // w_reduced is a rank vector

            // shift from alphabet [1..=n] to [0..n]
            w_reduced.iter_mut().for_each(|elem| *elem -= 1);

            //compute the inverse of the rank vector, aka a suffix array
            let mut sa = vec![0; w_reduced.len()];

            for i in 0..w_reduced.len() {
                sa[w_reduced[i]] = i;
            }

            debug_assert_eq!(
                SuffixArray(sa.clone()),
                suffix_array_naive(&w_reduced),
                "sa must be the suffix array to w_reduced"
            );

            SuffixArray(sa)
        } else {
            suffix_array(w_reduced.as_slice())
        }
    };
    let partial_rank = SuffixRank::new(&partial_sa);
    let enlarged_rank = partial_rank_to_enlarged_rank(&partial_rank, w, m.as_slice());

    let mut b0 = w
        .iter()
        .copied()
        .enumerate()
        .step_by(3)
        .collect::<Vec<(usize, C)>>();
    radsort::sort_by_key(b0.as_mut_slice(), |&(i, t_i)| (t_i, enlarged_rank[i + 1]));

    let sa = merge_sorted_lists(&b0, &partial_sa, &m, &enlarged_rank, w);
    debug_assert!(
        sa == suffix_array_naive(w).0,
        "w: {:?}\ncorrect sa: {:?}\n computed sa: {:?}",
        w,
        suffix_array_naive(w).0,
        &sa,
    );
    SuffixArray(sa)
}

/// Very simple reference implementation.
/// Used to compare against [`suffix_array`]
///
/// Takes quadratic time. On small input lengths, it may be faster because it is so simple.
pub fn suffix_array_naive<C: SACharacter>(w: &[C]) -> SuffixArray {
    let n = w.len();
    let mut sa: Vec<usize> = (0..n).collect();
    sa.sort_by(|&a, &b| w[a..].cmp(&w[b..]));
    SuffixArray(sa)
}

#[cfg(test)]
mod tests {
    use quickcheck::quickcheck;

    use super::*;

    #[test]
    fn build_meta_word_small() {
        let w = [1u32, 2, 3];
        let m = build_meta_word(&w[..]);
        assert_eq!(
            m,
            vec![
                KeyWithIndex {
                    key: (2, 3, 0),
                    meta_index: 0,
                    word_index: 1,
                    word_bucket: Bucket::B1
                },
                KeyWithIndex {
                    key: (3, 0, 0),
                    meta_index: 1,
                    word_index: 2,
                    word_bucket: Bucket::B2
                }
            ]
        )
    }

    #[test]
    fn build_meta_word_medium() {
        let w = [1u32, 2, 3, 4];
        let m = build_meta_word(&w[..]);
        assert_eq!(
            m,
            vec![
                KeyWithIndex {
                    key: (2, 3, 4),
                    meta_index: 0,
                    word_index: 1,
                    word_bucket: Bucket::B1
                },
                KeyWithIndex {
                    key: (3, 4, 0),
                    meta_index: 1,
                    word_index: 2,
                    word_bucket: Bucket::B2
                }
            ]
        )
    }

    #[test]
    fn reduce_word_to_integer_alphabet_small() {
        let m = vec![
            KeyWithIndex {
                key: (3u32, 0, 0),
                meta_index: 0,
                word_index: 1,
                word_bucket: Bucket::B1,
            },
            KeyWithIndex {
                key: (2, 3, 0),
                meta_index: 1,
                word_index: 2,
                word_bucket: Bucket::B2,
            },
        ];

        let w_reduced = reduce_word_to_integer_alphabet(&m);
        assert_eq!(w_reduced, vec![2, 1]);
    }

    #[test]
    fn build_meta_word_large() {
        let w = [1, 2, 3, 4, 5, 6];
        // meta-word = [(2,3,4), (5,6,0), (3,4,5), (6,0,0)]
        let expceted = vec![
            KeyWithIndex {
                key: (2u32, 3, 4),
                meta_index: 0,
                word_index: 1,
                word_bucket: Bucket::B1,
            },
            KeyWithIndex {
                key: (5, 6, 0),
                meta_index: 1,
                word_index: 4,
                word_bucket: Bucket::B1,
            },
            KeyWithIndex {
                key: (3, 4, 5),
                meta_index: 2,
                word_index: 2,
                word_bucket: Bucket::B2,
            },
            KeyWithIndex {
                key: (6, 0, 0),
                meta_index: 3,
                word_index: 5,
                word_bucket: Bucket::B2,
            },
        ];
        assert_eq!(expceted, build_meta_word(&w));
    }

    #[test]
    fn suffix_array_small() {
        let w = [1u32, 2, 3, 4];
        let sa = suffix_array(&w[..]);
        assert_eq!(sa, SuffixArray(vec![0, 1, 2, 3]))
    }

    #[test]
    fn suffix_array_88_1_1_88() {
        let w = [88usize, 1, 1, 88];
        let sa = suffix_array(&w[..]);
        assert_eq!(sa, SuffixArray(vec![1, 2, 3, 0]))
    }

    #[test]
    fn suffix_array_aba() {
        let s = "aba";
        let w: Vec<char> = s.chars().collect();
        let sa = suffix_array(w.as_slice());
        for &idx in sa.iter() {
            println!("{}", &s[idx..])
        }
        assert_eq!(SuffixArray(vec![2, 0, 1]), sa)
    }

    #[test]
    fn suffix_array_ababa() {
        let s = "ababa";
        let w: Vec<char> = s.chars().collect();
        let sa = suffix_array(w.as_slice());
        for &idx in sa.iter() {
            println!("{}", &s[idx..])
        }
        assert_eq!(SuffixArray(vec![4, 2, 0, 3, 1]), sa)
    }

    #[test]
    fn suffix_array_ababab() {
        let s = "ababab";
        let w: Vec<char> = s.chars().collect();
        let sa = suffix_array(w.as_slice());
        for &idx in sa.iter() {
            println!("{}", &s[idx..])
        }
        assert_eq!(SuffixArray(vec![4, 2, 0, 5, 3, 1]), sa)
    }

    #[test]
    fn suffix_array_yabbadabbado() {
        let s = "yabbadabbado";
        let w: Vec<char> = s.chars().collect();
        let sa = suffix_array(w.as_slice());
        for &idx in sa.iter() {
            println!("{}", &s[idx..])
        }
        assert_eq!(SuffixArray(vec![1, 6, 4, 9, 3, 8, 2, 7, 5, 10, 11, 0]), sa)
    }

    #[test]
    fn word_is_permutation_small_true() {
        let w = vec![3, 2, 4, 1];
        assert!(word_is_permutation(&w))
    }

    #[test]
    fn partial_rank_to_enlarged_rank_small() {
        let w = ['a', 'b', 'a'];
        let m = [
            KeyWithIndex {
                key: ('b', 'a', '\0'),
                meta_index: 0,
                word_index: 1,
                word_bucket: Bucket::B1,
            },
            KeyWithIndex {
                key: ('a', '\0', '\0'),
                meta_index: 1,
                word_index: 2,
                word_bucket: Bucket::B2,
            },
        ];
        let sa = SuffixArray(vec![1, 0]);
        let rank = SuffixRank::new(&sa);
        let expceted = vec![0, 2, 1, 0, 0, 0];
        assert_eq!(expceted, partial_rank_to_enlarged_rank(&rank, &w, &m))
    }

    #[test]
    fn partial_rank_to_enlarged_rank_medium() {
        let w = ['a', 'b', 'a', 'b', 'a'];
        let m = build_meta_word(&w);
        let m_reduced = reduce_word_to_integer_alphabet(&m);
        let sa = suffix_array_naive(&m_reduced);
        let rank = SuffixRank::new(&sa);
        let expceted = vec![0, 3, 2, 0, 1, 0, 0, 0];
        assert_eq!(expceted, partial_rank_to_enlarged_rank(&rank, &w, &m))
    }

    #[test]
    fn merge_sorted_lists_tiny() {
        let w = [1, 2, 3];
        // meta-word = [(2,3,0), (3,0,0)]
        let meta_word = [
            KeyWithIndex {
                key: (2u32, 3, 0),
                meta_index: 0,
                word_index: 1,
                word_bucket: Bucket::B1,
            },
            KeyWithIndex {
                key: (3, 0, 0),
                meta_index: 1,
                word_index: 2,
                word_bucket: Bucket::B2,
            },
        ];
        // reduced word = [0, 1] -- already suffix array
        let partial_sa = SuffixArray(vec![0, 1]);
        // partial rank = [0, 2, 1, 3]
        let enlarged_rank = [0, 0, 1, 0, 0, 0];

        let b0 = [(0, 1)];

        let expected = vec![0, 1, 2];

        assert_eq!(
            expected,
            merge_sorted_lists(&b0, &partial_sa, &meta_word, &enlarged_rank, &w)
        )
    }

    #[test]
    fn merge_sorted_lists_small() {
        let w = [1, 2, 3, 4, 5, 6];
        // meta-word = [(2,3,4), (5,6,0), (3,4,5), (6,0,0)]
        let meta_word = [
            KeyWithIndex {
                key: (2u32, 3, 4),
                meta_index: 0,
                word_index: 1,
                word_bucket: Bucket::B1,
            },
            KeyWithIndex {
                key: (5, 6, 0),
                meta_index: 1,
                word_index: 4,
                word_bucket: Bucket::B1,
            },
            KeyWithIndex {
                key: (3, 4, 5),
                meta_index: 2,
                word_index: 2,
                word_bucket: Bucket::B2,
            },
            KeyWithIndex {
                key: (6, 0, 0),
                meta_index: 3,
                word_index: 5,
                word_bucket: Bucket::B2,
            },
        ];
        // reduced word = [0, 2, 1, 3] -- already suffix array
        let partial_sa = SuffixArray(vec![0, 2, 1, 3]);
        println!("partial rank:{:?}", SuffixRank::new(&partial_sa));
        // partial rank = [0, 2, 1, 3]
        let enlarged_rank = [0, 0, 2, 0, 1, 3, 0, 0, 0];

        let b0 = [(0, 1), (3, 4)];

        let expected = vec![0, 1, 2, 3, 4, 5];

        assert_eq!(
            expected,
            merge_sorted_lists(&b0, &partial_sa, &meta_word, &enlarged_rank, &w)
        )
    }

    quickcheck! {
        fn suffix_array_linear_vs_naive(w: Vec<u8>) -> bool {
           let s = w.as_slice();
           //skip cases where there is a 0 in the array
           if s.iter().any(|&x| x==0) {
               return true
           }

           suffix_array(s) == suffix_array_naive(s)

        }

        /// two sorted lists should get merged into one sorted list
        fn merge_sorted_lists_generic_does_merge_correctly(a: Vec<u32>, b: Vec<u64>) -> bool {
            let mut a = a;
            let mut b = b;
            a.sort();
            b.sort();

            let mut c = {
                let mut b_clone = b.clone();
                let mut a_clone = a.iter().map(|&x| x as u64).collect::<Vec<_>>();
                a_clone.append(&mut b_clone);
                a_clone
            };
            c.sort();

            c == merge_sorted_lists_generic(a.into_iter(), b.into_iter(), |&x, &y| x as u64 <= y, |x| x as u64, |y| y)

        }
    }
}
