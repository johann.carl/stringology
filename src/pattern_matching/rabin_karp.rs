//! Collection of functions around Rabin Karp pattern matching.

use katexit::katexit;
use primal;
use rand::distributions::Uniform;
use rand::{thread_rng, Rng};

/// number of possible values in [`u8`].
const D: u64 = 256;

#[katexit]
/// compute the Rabin-Karp Hash, using the Horner scheme.
///
/// `text`: text to hash
///
/// `q`: modulo
///
/// complexity: $\\mathcal{O}(|\\text{text}|)$
pub fn build_hash(text: &[u8], q: u64) -> u64 {
    let d: u64 = 256;

    text.iter()
        .fold(0u64, |partial_hash, &t| (partial_hash * d + (t as u64)) % q)
}

/// returns -x, modulo q
#[inline]
fn minus_mod(x: u64, q: u64) -> u64 {
    q - (x % q)
}

#[katexit]
/// consider the text slice $t_i, \\dots, t_j, t_{j+1}$.
///
/// `t_out`: $t_i$
///
/// `t_in`: $t_{j+1}$
///
/// `hash`: rabin karp hash of the slice $t_i, \\dots, t_j$
///
/// `d_pow`: should equal $d^{m-1}$ with $m = j - i$ the length of the hashing window, $d=256$ the size of
/// the alphabet
///
/// returns: a hash over the slice $t_{i+1}, \\dots, t_{j+1}$
///
/// complexity: $\\mathcal{O}(1)$
pub fn roll_hash(t_out: u8, t_in: u8, hash: u64, q: u64, d_pow: u64) -> u64 {
    // equals (-d_pow * t_out as u64) modulo q
    //let minus_dpow_times_tout = q - ((d_pow * t_out as u64) % q);

    (D * (hash + minus_mod(d_pow * (t_out as u64), q)) + (t_in as u64)) % q
}

#[katexit]
/// returns $x^p \\mod q$
///
/// complexity: $\\mathcal{O}(p)$
fn pow_mod(x: u64, p: usize, q: u64) -> u64 {
    (0..p).fold(1, |x_pow, _| (x_pow * x) % q)
}

#[katexit]
/// chooses a good q for Rabin Karp hashing:
///
/// some random element from $\\{q | q \\in [1, p \cdot (t-p+1) \cdot \\log_2(256)], q \\text{ is
/// prime}\\}$
/// with $p = |\\text{pattern}|, t = |\\text{text}|$.
///
/// Expedted complexity: $\\mathcal{O}(n)$
pub fn choose_good_q(text: &[u8], pattern: &[u8]) -> u64 {
    let mut rng = thread_rng();
    let p = pattern.len() as u64;
    let t = text.len() as u64;
    let log_d = 8;
    let distribution = Uniform::<u64>::new_inclusive(1, p * (t - p + 1) * log_d);

    let mut q = 4;
    while !primal::is_prime(q) {
        q = rng.sample(distribution);
    }
    q
}

#[katexit]
/// searches for the first occourence of `pattern` in `text`,
/// using the [Rabin-Karp algorithm](https://en.wikipedia.org/wiki/Rabin%E2%80%93Karp_algorithm).
///
/// `q`: modulo
///
/// complexity: $\\mathcal{O}(|\\text{text}| + |\\text{pattern}|)$ with high propability
pub fn rabin_karp(text: &[u8], pattern: &[u8], q: u64) -> Option<usize> {
    if pattern.is_empty() {
        eprintln!("pattern empty");
        return None;
    }

    let m = pattern.len();
    let d_pow = pow_mod(D, m - 1, q);

    if text.len() < m {
        eprintln!("text too short");
        return None;
    }

    let p_hash = build_hash(pattern, q);
    let mut t_hash = build_hash(&text[0..m], q);
    if p_hash == t_hash && pattern == &text[0..m] {
        return Some(0);
    }
    for (idx, window) in text.windows(m + 1).enumerate() {
        t_hash = roll_hash(window[0], window[m], t_hash, q, d_pow);
        if p_hash == t_hash && pattern == &window[1..=m] {
            return Some(idx + 1);
        }
    }
    // if no occourence was found
    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn build_hash_empty() {
        let text = [];
        let q = 1175;

        let expected = 0;
        assert_eq!(build_hash(&text, q), expected);
    }

    #[test]
    fn build_hash_supereasy() {
        let text = b"b";
        let q = 17;

        let expected = b'b' as u64 % q;
        assert_eq!(build_hash(text, q), expected);
    }

    #[test]
    fn build_hash_easy() {
        let text = [5, 3];
        let q = u64::MAX;

        let expected = 5 * 256 + 3;
        assert_eq!(build_hash(&text, q), expected);
    }

    #[test]
    fn build_hash_medium() {
        let text = [5, 3];
        let q = 1175;

        let expected = (5 * 256 + 3) % q; // = 108
        assert_eq!(build_hash(&text, q), expected);
    }

    #[test]
    fn build_hash_longer() {
        let text = [5, 3, 7];
        let q = 1175;

        let expected = (5 * 256 * 256 + 3 * 256 + 7) % q;
        assert_eq!(build_hash(&text, q), expected);
    }

    #[test]
    fn roll_hash_easy() {
        let q = 173;
        let text = b"a";
        let m = text.len();
        let hash = build_hash(text, q);
        let t_out = b'a';
        let t_in = b'b';
        let d_pow = D.pow((m - 1) as u32);
        assert_eq!(roll_hash(t_out, t_in, hash, q, d_pow), build_hash(b"b", q))
    }

    #[test]
    fn roll_hash_medium() {
        let q = 173;
        let text = b"abcd";
        let m = text.len();
        let hash = build_hash(text, q);
        let t_out = b'a';
        let t_in = b'e';
        let d_pow = D.pow((m - 1) as u32);
        assert_eq!(
            roll_hash(t_out, t_in, hash, q, d_pow),
            build_hash(b"bcde", q)
        )
    }

    #[test]
    fn pow_mod_zero() {
        assert_eq!(pow_mod(14, 0, 7), 1)
    }

    #[test]
    fn pow_mod_easy() {
        assert_eq!(pow_mod(14, 2, 7), (14 * 14) % 7)
    }

    #[test]
    fn pow_mod_medium() {
        let x: u64 = 15;
        let p: u8 = 10;
        let q: u64 = 7;
        assert_eq!(pow_mod(x, p as usize, q), (x.pow(p as u32)) % q)
    }

    #[test]
    fn rabin_karp_pattern_easy() {
        let text = b"aba";
        let pattern = b"b";
        let q = 17;
        assert_eq!(rabin_karp(text, pattern, q), Some(1))
    }

    #[test]
    fn rabin_karp_medium() {
        let text = b"yabadabadoo";
        let pattern = b"dab";
        let q = 173;
        assert_eq!(rabin_karp(text, pattern, q), Some(4))
    }

    #[test]
    #[allow(clippy::redundant_closure)]
    fn choose_good_q_only_primes() {
        let text = [127; 10000];
        let pattern = b"slightly longer pattern";
        let samples = (0..100).map(|_| choose_good_q(&text, pattern));
        let expected = vec![true; 100];
        assert_eq!(
            samples.map(|q| primal::is_prime(q)).collect::<Vec<bool>>(),
            expected
        )
    }
}
