//! Provides the Knuth Morris Pratt algorithm for pattern matching.

/// computes a vector p, s.t. for
/// `k := p[q]` there holds:
///
/// `pattern[0..k]` is the longest prefix of `pattern[0..q]`,
/// that is also a suffix of `pattern[0..q]` and `k < q`.
fn prefix_function<C: PartialEq>(pattern: &[C]) -> Vec<usize> {
    let m = pattern.len();
    let mut p = vec![0; m];
    let mut k = 0;

    for q in 1..m {
        while k > 0 && pattern[k] != pattern[q] {
            debug_assert!(
                k > p[k - 1],
                "p should decrease k, else an infinite loop might occour"
            );
            k = p[k - 1];
        }
        if pattern[k] == pattern[q] {
            k += 1;
        }
        p[q] = k;
    }
    p
}

/// Does pattern matching, using the
/// [KMP algorithm](https://en.wikipedia.org/wiki/Knuth%E2%80%93Morris%E2%80%93Pratt_algorithm).
///
/// Finds the first occourence of `pattern` in `text`, if it exists.
/// Returns the starting index.
pub fn kmp<C: PartialEq>(text: &[C], pattern: &[C]) -> Option<usize> {
    let p = prefix_function(pattern);
    let m = pattern.len();

    let mut q = 0;
    for (i, t) in text.iter().enumerate() {
        while q > 0 && pattern[q] != *t {
            debug_assert!(
                q > p[q - 1],
                "p should decrease q, else an infinite loop might occour"
            );
            q = p[q - 1];
        }
        if pattern[q] == *t {
            q += 1;
        }
        if q == m {
            return Some(i + 1 - m);
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use quickcheck::quickcheck;

    use crate::pattern_matching::naive_pattern_matching;

    use super::*;

    #[test]
    fn small_prefix_function() {
        let pattern = b"aaab";
        let p = prefix_function(pattern);

        assert_eq!(p, vec![0, 1, 2, 0]);
    }

    #[test]
    fn no_good_prefix_function() {
        let pattern = b"abcde";
        let p = prefix_function(pattern);

        assert_eq!(p, vec![0, 0, 0, 0, 0])
    }

    #[test]
    fn kmp_no_find() {
        let text = b"xyzaaababcdef";
        let pattern = b"wxyz";
        assert_eq!(kmp(text, pattern), None)
    }

    #[test]
    fn small_kmp() {
        let pattern = b"a";
        let text = b"bbab";

        assert_eq!(kmp(text, pattern), Some(2));
    }
    #[test]
    fn medium_kmp() {
        let pattern = b"abc";
        let text = b"xyzaaababcdef";

        assert_eq!(kmp(text, pattern), Some(7));
    }

    #[test]
    fn non_ascii_pattern_matching() {
        let text: Vec<_> = "Добрый день".chars().collect();
        let pattern: Vec<_> = "день".chars().collect();

        assert_eq!(kmp(&text, &pattern), Some(7));
    }

    #[test]
    fn emoji_pattern_matching() {
        let text: Vec<_> = "🌟🎉🍕😊🎈".chars().collect();
        let pattern: Vec<_> = "😊".chars().collect();

        assert_eq!(kmp(&text, &pattern), Some(3));
    }

    quickcheck! {
        fn kmp_agrees_with_naive_pattern_matching(text: Vec<u8>, pattern: Vec<u8>) -> bool {
            naive_pattern_matching(&text, &pattern) == kmp(&text, &pattern)
        }
    }
}
