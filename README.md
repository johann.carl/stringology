# stringology

This crate aims to be a collection of several algorithms from the field of stringology.
The project is not finished, more features will be added in the future.
At the moment, `stringology` offers algorithms on the following topics:

- pattern matching
- suffix arrays

## Benchmarks


`stringology` uses [criterion](https://crates.io/crates/criterion) for benchmarking.
To execute the benchmarks run

```bash
cargo bench 
```

## License

[LGPL-3.0-only](LICENSE)

